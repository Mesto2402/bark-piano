r = 1.05949;
a = 33488.08;

[data, fs] = audioread('Bark2.wav');

tutorial = 'A=DO, W=DO#, S=RE, E=RE#, D=MI, F=FA, T=FA#, G=SOL, Y=SOL#, H=LA, U=LA#, J=SI, K=DO; Finalizar: z';
input(tutorial);

prompt = 'Nota: ';
x = input(prompt, 's');

while ~(x == 'z' || x == 'Z')
    if (x == 'A' || x == 'a')
        sound(data,a);
    elseif (x == 'W' || x == 'w')
        sound(data,a*r^1);
    elseif (x == 'S' || x == 's')
        sound(data,a*r^2);
    elseif (x == 'E' || x == 'e')
        sound(data,a*r^3);
    elseif (x == 'D' || x == 'd')
        sound(data,a*r^4);
    elseif (x == 'F' || x == 'f')
        sound(data,a*r^5);
    elseif (x == 'T' || x == 't')
        sound(data,a*r^6);
    elseif (x == 'G' || x == 'g')
        sound(data,a*r^7);
    elseif (x == 'Y' || x == 'y')
        sound(data,a*r^8);
    elseif (x == 'H' || x == 'h')
        sound(data,a*r^9);
    elseif (x == 'U' || x == 'u')
        sound(data,a*r^10);
    elseif (x == 'J' || x == 'j')
        sound(data,a*r^11);
    elseif (x == 'K' || x == 'k')
        sound(data,a*r^12);
    elseif (x == 'O' || x == 'o')
        sound(data,a*r^13);
    elseif (x == 'L' || x == 'l')
        sound(data,a*r^14);
    end
    
    prompt = 'Nota: ';
    x = input(prompt, 's');
end